export interface IUser {
  id: number;
  first_name: string;
  last_name: string;
  email?: string;
  account_status?: boolean;
  registration_datetime?: string;
  tfa_enabled?: boolean;
  token?: string;
}
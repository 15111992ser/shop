import { OS } from "../constants/OS";

export interface IPhone {
  id: number;
  name: string;
  price: number;
  memory: number;
  ram: number;
  diagonal: number;
  battery: number;
  camera: number;
  os: OS;
  phone_images: {
    images: string[];
  };
  brand_id: number;
  brand_name: string;
  rating: number;
  percentage: number;
  start_time: Date;
  end_time: Date;
}

export interface IFilters {
  take?: number;
  skip?: number;
  name?: string;
  min_price?: number;
  max_price?: number;
  memories?: number[];
  ram?: number[];
  diagonals?: number[];
  batteries?: number[];
  cameras?: number[];
  os?: OS[];
  brands?: number[];
}

import { IUser } from "./user";

export interface IReview {
  id: number;
  user: IUser;
  rating: number;
  review_rating: number;
  user_rating: boolean;
  comment: string;
  datetime: Date;
  updated: Date;
  __typename: string;
}

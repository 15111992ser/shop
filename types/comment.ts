import { IUser } from "./user";

export interface IComment {
  id: number;
  user: IUser;
  comment: string;
  datetime: string;
  comment_rating: number;
  user_rating: boolean;
  updated: string;
  answers: number;
}

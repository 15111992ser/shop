export const calcSale = (price: number, sale: number): number => {
  return price - (sale / 100) * price;
};

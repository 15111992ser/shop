export const getFormatDate = (data: Date | number) => {
    const d = new Date(data);
    
    const date = d.getDate().toString().padStart(2, "0");
    const month = (d.getMonth() + 1).toString().padStart(2, "0");
    const year = d.getFullYear();
  
    return `${date}.${month}.${year}`;
  };
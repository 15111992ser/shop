import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import cookie from "js-cookie";

const httpLink = createHttpLink({
  uri: "http://localhost:3003/graphql",
  credentials: "same-origin",
});

const authLink = setContext((_, { headers }) => {
  if (typeof window !== "undefined") {
    const token = cookie.get("access-token");
    return {
      headers: {
        ...headers,
        "access-token": token || "",
      },
    };
  }
});

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        getComments: {
          keyArgs: ["data", ["review_id"]],
          merge(existing = [], incoming) {
            return {
              ...incoming,
              comments: [...(existing?.comments ?? []), ...incoming.comments],
            };
          },
        },
        getReplyComments: {
          keyArgs: ["data", ["comment_id"]],
          merge(existing , incoming) {
            return { ...incoming, comments: [...existing?.comments ?? [], ...incoming.comments] };
          },
        },
        getPhones: {
          keyArgs: false,
          merge(existing, incoming) {
            return { ...incoming, phones: [...(existing?.phones ?? []), ...incoming.phones] };
          },
        },
      },
    },
  },
});

export const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache,
  connectToDevTools: true,
});

import { gql } from "@apollo/client";

export const GET_USER_INFO = gql`
  query {
    getUserInfo {
      id
      first_name
      last_name
      email
      account_status
      registration_datetime
      tfa_enabled
      token
    }
  }
`;

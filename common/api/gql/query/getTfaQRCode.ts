import { gql } from "@apollo/client";

export const GET_QR_CODE = gql`
  query {
    getTfaQRCode {
      qr
    }
  }
`;

import { gql } from "@apollo/client";

export const GET_PHONE = gql`
  query ($id: Int!) {
    getPhone(id: $id) {
      id
      rating
      name
      price
      memory
      ram
      diagonal
      battery
      camera
      os
      phone_images {
        images
      }
      brand_id
      brand_name
      percentage
      start_time
      end_time
    }
  }
`;

import { gql } from "@apollo/client";

export const TFA_AUTHORIZATION = gql`
  query ($token: String!) {
    tfaAuthorization(token: $token) {
      id
      first_name
      last_name
      email
      account_status
      registration_datetime
      tfa_enabled
      token
    }
  }
`;

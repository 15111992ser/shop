import { gql } from "@apollo/client";

export const GET_PHONES = gql`
  query ($data: GetPhonesByFilters!) {
    getPhones(data: $data) {
      phones {
        id
        name
        price
        memory
        ram
        diagonal
        battery
        camera
        os
        phone_images {
          images
        }
        brand_id
        brand_name
        rating
        percentage
        start_time
        end_time
      }
      total
    }
  }
`;

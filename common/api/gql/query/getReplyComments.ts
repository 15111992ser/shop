import { gql } from "@apollo/client";

export const GET_REPLY_COMMENTS = gql`
  query ($data: GetReplyComments!) {
    getReplyComments(data: $data) {
      comments {
        id
        user {
          id
          first_name
          last_name
        }
        comment
        datetime
        comment_rating
        user_rating
        updated
        answers
      }
      total
    }
  }
`;

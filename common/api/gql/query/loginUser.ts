import { gql } from "@apollo/client";

export const LOGIN_USER = gql`
  query ($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      first_name
      last_name
      email
      account_status
      registration_datetime
      tfa_enabled
      token
    }
  }
`;

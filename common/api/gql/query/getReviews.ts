import { gql } from "@apollo/client";

export const GET_REVIEWS = gql`
  query ($data: GetPhoneReviews!) {
    getReviews(data: $data) {
      reviews {
        id
        user {
          id
          first_name
          last_name
        }
        rating
        review_rating
        user_rating
        comment
        datetime
        updated
      }
      total
    }
  }
`;

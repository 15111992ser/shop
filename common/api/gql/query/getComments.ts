import { gql } from "@apollo/client";

export const GET_COMMENTS = gql`
  query ($data: GetReviewComments!) {
    getComments(data: $data) {
      comments {
        id
        user {
          id
          first_name
          last_name
        }
        answers
        comment
        datetime
        comment_rating
        user_rating
        updated
      }
      total
    }
  }
`;

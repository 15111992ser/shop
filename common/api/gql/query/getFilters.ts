import { gql } from "@apollo/client";

export const GET_FILTERS = gql`
  query ($data: GetFiltersDto!) {
    getFilters(data: $data) {
      brands {
        id
        phones
        name
      }
      memories {
        phones
        memory
      }
      ram {
        phones
        ram
      }
      diagonals {
        phones
        diagonal
      }
      batteries {
        phones
        battery
      }
      cameras {
        phones
        camera
      }
      os {
        phones
        os
      }
      price {
        min
        max
      }
    }
  }
`;

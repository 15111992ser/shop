import { gql } from "@apollo/client";

export const UPDATE_REVIEW = gql`
  mutation ($data: UpdateReview!) {
    updateReview(data: $data) {
      message
    }
  }
`;

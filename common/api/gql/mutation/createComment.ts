import { gql } from "@apollo/client";

export const CREATE_COMMENT = gql`
  mutation ($data: CreateComment!) {
    createComment(data: $data) {
      message
    }
  }
`;

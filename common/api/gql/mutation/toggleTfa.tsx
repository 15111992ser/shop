import { gql } from "@apollo/client";

export const TOOGLE_T_FA = gql`
  mutation ($token: String!) {
    toggleTfa(token: $token) {
      message
    }
  }
`;

import { gql } from "@apollo/client";

export const CREATE_REVIEW = gql`
  mutation ($data: CreateReview!) {
    createReview( data: $data,) {
      message
    }
  }
`;

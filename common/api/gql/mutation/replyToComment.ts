import { gql } from "@apollo/client";

export const REPLY_TO_COMMENT = gql`
  mutation ($data: ReplyToComment!) {
    replyToComment(data: $data) {
      message
    }
  }
`;

import { gql } from "@apollo/client";

export const DELETE_REVIEW = gql`
  mutation ($id: Int!) {
    deleteReview(id: $id) {
      message
    }
  }
`;

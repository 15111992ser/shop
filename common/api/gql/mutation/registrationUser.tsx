import { gql } from "@apollo/client";

export const REGISTER_USER = gql`
mutation($data: Registration!){
  registration(data: $data){
    id
    first_name
    last_name
    email
    account_status
    registration_datetime
    tfa_enabled
    token
  }
}
`;

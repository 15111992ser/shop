import { gql } from "@apollo/client";

export const RATE_REVIEW = gql`
  mutation ($data: RateReview!) {
    rateReview(data: $data) {
      message
    }
  }
`;

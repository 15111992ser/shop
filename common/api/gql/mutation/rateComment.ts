import { gql } from "@apollo/client";

export const RATE_COMMENT = gql`
  mutation ($data: RateComment!) {
    rateComment(data: $data) {
      message
    }
  }
`;

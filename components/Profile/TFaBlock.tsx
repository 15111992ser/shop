import { Button, Paper, CardMedia, Stack, TextField, Typography, Grid } from "@mui/material";
import ErrorIcon from "@mui/icons-material/Error";
import { useEffect, useState } from "react";
import { useMutation, useQuery } from "@apollo/client";
import { TOOGLE_T_FA } from "../../common/api/gql/mutation/toggleTfa";
import { GET_QR_CODE } from "../../common/api/gql/query/getTfaQRCode";
import useAuthContext from "../../hooks/useAuthContext";
import { ShopTextField } from "../ShopTextField";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

interface ISecretCode {
  secretCode: string;
}

const schema = yup.object().shape({
  secretCode: yup.string().required("Secret code is required"),
});

export const TFaBlock = () => {
  const { refetchUser, user } = useAuthContext();
  const [qrCode, setQrCode] = useState("");
  const { data } = useQuery(GET_QR_CODE);
  const [toogleTfa, { error: errorToogleTfa, loading }] = useMutation(TOOGLE_T_FA);
  let qr = data?.getTfaQRCode.qr;

  const {
    handleSubmit,
    reset,
    control,
    formState: { errors },
  } = useForm<ISecretCode>({
    resolver: yupResolver(schema),
    defaultValues: {
      secretCode: "",
    },
  });

  useEffect(() => {
    if (qr) {
      setQrCode(qr.slice(qr.indexOf("secret") + 7));
    }
  }, [qr]);

  const handleToogleTfa: SubmitHandler<ISecretCode> = async (data) => {
    await toogleTfa({
      variables: { token: data.secretCode },
    });
    refetchUser();
    reset();
  };

  return (
    <Paper sx={{ p: 2 }}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Typography variant="h6" component="h4">
            Two-Factor Authentication (2FA)
          </Typography>

          <Grid container spacing={1}  >
            <Grid item xs={12} sm={4} >
                <CardMedia
                  component="img"
                  sx={{ width: {xs: "50%", sm: "100%"}, border: "1px solid gray", borderRadius: "4px", m: "auto" }}
                  image={qr}
                  alt="QR code"
                />
            </Grid>
            <Grid item xs={12} sm={8} >
              <Stack spacing={2} justifyContent="flex-end" flexGrow={1}>
                <Typography variant="subtitle2" sx={{ marginBottom: -1 }}>
                  2FA KEY (OR CODE)
                </Typography>
                <TextField
                  fullWidth
                  variant="outlined"
                  id="outlined-name"
                  size="small"
                  value={qrCode}
                />
                <form onSubmit={handleSubmit(handleToogleTfa)}>
                  {errorToogleTfa?.message && (
                    <Typography sx={{ color: "red", fontSize: "14px" }}>
                      {errorToogleTfa?.message}
                    </Typography>
                  )}
                  <ShopTextField
                    control={control}
                    name="secretCode"
                    error={!!errors.secretCode?.message}
                    helperText={errors.secretCode?.message}
                    label="Enter code"
                    variant="outlined"
                    size="small"
                  />
                  <Button variant="outlined" type="submit" disabled={loading} sx={{ mt: 2 }}>
                    {user?.tfa_enabled ? "Disable" : "Enable"}
                  </Button>
                </form>
              </Stack>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack spacing={2} height="100%" justifyContent="center">
            <ErrorIcon color="primary" fontSize="large" />
            <Typography variant="subtitle1">
              To activate two-factor authentication your account, scan the QR code using the
              authenicator application (Google Authenticator or Twilio) at your mobile. You may also
              enter the 2TFA Key manually at your device. Afterwards, enter the token code you see
              at your mobile in the field to activate 2FA on your account.
            </Typography>
          </Stack>
        </Grid>
      </Grid>
    </Paper>
  );
};

import { useLazyQuery } from "@apollo/client";
import { GET_PHONES } from "../../common/api/gql/query/getPhones";
import { useEffect, useState } from "react";
import { IFilters, IPhone } from "../../types/phones";
import { TextField, Typography, Paper, Pagination, Button, Grid } from "@mui/material";
import { Stack } from "@mui/system";
import { phone } from "../../constants/phones";
import ListPhonesItem from "./ListPhonesItem";

interface IListPhonesProps {
  defaultFilters: IFilters;
  handleChangeFilter: (selectedFilters: IFilters) => void;
}

const ListPhones: React.FC<IListPhonesProps> = ({ defaultFilters, handleChangeFilter }) => {
  const [searchValue, setSearchValue] = useState("");
  const [page, setPage] = useState(1);
  const [getPhones, { loading: loadingPhone, data: getPhonesData, fetchMore, refetch }] =
    useLazyQuery(GET_PHONES);

  const phones = getPhonesData?.getPhones.phones;
  const totalPhones = getPhonesData?.getPhones.total;
  const countPages = Math.ceil((totalPhones || 0) / phone.TAKE);
  
  const handleChangePage = (event: React.ChangeEvent<unknown>, page: number) => {
    const currentPage = page;
    const skipPhones = (currentPage - 1) * phone.TAKE;
    refetch({ data: { ...defaultFilters, take: phone.TAKE, skip: skipPhones } });
    setPage(page);
  };

  const handleShowMore = () => {
    fetchMore({
      variables: { data: { ...defaultFilters, take: phone.TAKE, skip: page * phone.TAKE } },
    });
    setPage((prev) => prev + 1);
  };
  
  useEffect(() => {
    getPhones({ variables: { data: defaultFilters } });
  }, []);

  useEffect(() => {
    handleChangeFilter({ name: searchValue });
  }, [searchValue]);

  useEffect(() => {
    refetch({ data: defaultFilters  });
  }, [defaultFilters, refetch]);

  return (
    <>
      <Paper sx={{ p: 2, mb: 1, ml: 2, mt: 2, width: "100%" }}>
        <Typography variant="h5">Total found phones {totalPhones} </Typography>
      </Paper>
      <TextField
        placeholder="Search phones"
        variant="standard"
        value={searchValue}
        size="small"
        fullWidth
        margin="normal"
        sx={{ ml: 2 }}
        onChange={(e) => {
          setSearchValue(e.target.value);
        }}
      />
      {phones?.length && !loadingPhone ? (
        phones?.map((phone: IPhone, i: any) => {
          return <ListPhonesItem phone={phone} key={i} />;
        })
      ) : (
        <Stack direction="row" width="100%" height="100%" justifyContent="center">
          <Typography variant="h4">Sorry, no products found</Typography>
        </Stack>
      )}
      <Grid item xs={12}>
        {totalPhones > phone.TAKE && (
          <Stack direction="column" alignItems="center" spacing={2} mt={2}>
            <Button onClick={handleShowMore} disabled={countPages === page}>
              Show more
            </Button>
            <Pagination
              count={countPages}
              variant="outlined"
              shape="rounded"
              page={page}
              onChange={handleChangePage}
            />
          </Stack>
        )}
      </Grid>
    </>
  );
};

export default ListPhones
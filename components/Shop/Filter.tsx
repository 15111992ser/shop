import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Checkbox,
  FormControlLabel,
  Slider,
  Stack,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { IFilters } from "../../types/phones";

import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";

interface IFilterItem {
  __typename: string;
  [key: string]: string | number;
}

type labelFilterNamesKeys = keyof typeof labelFilterNames;

const labelFilterNames = {
  GroupByBrands: "name",
  GroupByMemory: "memory",
  GroupByRam: "ram",
  GroupByDiagonal: "diagonal",
  GroupByBattery: "battery",
  GroupByCamera: "camera",
  GroupByOs: "os",
};

type filterValuesKeys = keyof typeof filterValues;

const filterValues = {
  ...labelFilterNames,
  GroupByBrands: "id",
};

interface ISelectedFilters {
  handleChange: (selectedFilters: IFilters) => void;
  filters: any
}

const Filter: React.FC<ISelectedFilters> = ( { handleChange: handleChangeExternal, filters }) => {

  const filterList = Object.keys(filters || {})?.filter((el) => el !== "__typename");
  const [selectedFilters, setSelectedFilters] = useState<any>({});
  const [rangePrice, setRangePrice] = useState<number[]>([0, Infinity]);
  const handleChangePrice = (event: Event, newValue: number | number[]) => {
    setRangePrice(newValue as number[]);
  };

  const createPriceRange = () => {
    setSelectedFilters({ ...selectedFilters, min_price: rangePrice[0], max_price: rangePrice[1] });
  };

  const handleChangeCheckBox = (event: React.ChangeEvent<HTMLInputElement>, filterName: string) => {
    const value = Number(event.target.value) || event.target.value;

    if (selectedFilters[filterName]?.includes(value)) {
      setSelectedFilters({
        ...selectedFilters,

        [filterName]: selectedFilters[filterName].filter((el: any) => el !== value),
      });
    } else {
      setSelectedFilters({
        ...selectedFilters,

        [filterName]: [...(selectedFilters[filterName] ?? []), value],
      });
    }
  };

  useEffect(() => {
    handleChangeExternal(selectedFilters);
  }, [selectedFilters]);

  const renderFilters = (payload: IFilterItem | IFilterItem[], filterName: string) => {
    const currentPayload = Array.isArray(payload) ? payload[0] : payload;

    switch (currentPayload.__typename) {
      case "PriceRange":
        return (
          <Stack direction="row" spacing={2}>
            <Slider
              getAriaLabel={() => "Price range"}
              value={rangePrice}
              onChange={handleChangePrice}
              valueLabelDisplay="auto"
              min={currentPayload.min as number}
              max={currentPayload.max as number}
            />
            <Button onClick={createPriceRange} size="small">
              OK
            </Button>
          </Stack>
        );

      default:
        if (Array.isArray(payload)) {
          return payload.map((el, i) => {
            return (
              <FormControlLabel
                key={i}
                sx={{ width: "100%" }}
                control={
                  <Checkbox
                    onChange={(e) => {
                      handleChangeCheckBox(e, filterName);
                    }}
                    value={el[filterValues[el.__typename as filterValuesKeys]]}
                  />
                }
                label={el[labelFilterNames[el.__typename as labelFilterNamesKeys]]}
              />
            );
          });
        }
    }
  };

  return (
    <>
      {filterList.map((filter) => {
        return (
          <Accordion key={filter}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography variant="body1"> {filter.toUpperCase()} </Typography>
            </AccordionSummary>
            <AccordionDetails>{renderFilters(filters[filter], filter)}</AccordionDetails>
          </Accordion>
        );
      })}
    </>
  );
};

export  default Filter


import { Box, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { IFilters } from "../../types/phones";
import { phone } from "../../constants/phones";
import Filter from "./Filter";
import ListPhones from "./ListPhones";

interface IShopProps{
  filters: IFilters;
}

const Shop: React.FC<IShopProps> = ({ filters }) => {
  const [defaultFilters, setDefaultFilters] = useState<IFilters>({
    take: phone.TAKE,
    skip: 0,
    name: "",
    min_price: 0,
    max_price: phone.MAX_PRICE,
  });

  const handleChangeFilter = (selectedFilters: IFilters) => {
    const newFilters = { ...defaultFilters, ...selectedFilters };
    const params: Record<string, any> = {};

    for (const key in newFilters) {
      const value = newFilters[key as keyof typeof newFilters];
      if (
        (Array.isArray(value) && value.length) ||
        typeof value === "string" ||
        typeof value === "number"
      ) {
        params[key] = value;
      }
    }

    setDefaultFilters(params as IFilters);
  };

  return (
    <Box>
      <Typography variant="h3" mb={2}>
        Smartphones
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={3}>
          <Filter handleChange={handleChangeFilter} filters={filters}/>
        </Grid>
        <Grid item xs={12} md={9}>
          <Grid container spacing={2}>
            <ListPhones defaultFilters={defaultFilters} handleChangeFilter={handleChangeFilter} />
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

export  default Shop
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Stack,
  Typography,
  Link as LinkMUI,
  Rating,
} from "@mui/material";
import { IPhone } from "../../types/phones";
import { numberWithSpaces } from "../../utils/numberWithSpaces";
import { calcSale } from "../../utils/calcSale";
import { red } from "@mui/material/colors";
import { Sale } from "../Sale";
import Link from "next/link";

interface IListPhonesItemProps {
  phone: IPhone;
}

const ListPhonesItem: React.FC<IListPhonesItemProps> = ({ phone }) => {
  
  return (
    <Grid item xs={12} sm={6} md={4} lg={3}>
      <Link href={`phone/${phone.id}`} passHref> 
        <LinkMUI color="inherit" sx={{ textDecoration: "none" }}>
          <Card sx={{ position: "relative" }}>
            {phone.percentage && <Sale sale={phone.percentage} />}
            <Stack
              sx={{ width: "210px", padding: 2, margin: "0 auto" }}
              direction="row"
              alignItems="center"
              justifyContent="center"
            >
              <CardMedia
                sx={{ objectFit: "contain" }}
                height="250"
                component="img"
                image={phone.phone_images.images[0]}
                alt={phone.name}
              />
            </Stack>
            <CardContent sx={{ height: "100px", "&.MuiCardContent-root": { pt: 0 } }}>
              <Stack direction="row" justifyContent="space-between" alignItems="center" mb={2}>
                <Typography variant="h5" component="div">
                  {phone.brand_name}
                </Typography>
                <Rating
                  name="read-only"
                  value={phone.rating}
                  readOnly
                  size="small"
                  precision={0.1}
                />
              </Stack>
              <Typography gutterBottom variant="body2" color="text.secondary">
                {phone.name}
              </Typography>
            </CardContent>
            <CardActions>
              <Stack
                direction="row"
                alignItems="center"
                justifyContent="space-between"
                width="100%"
                height="40px"
              >
                <Button size="small">Add to Card</Button>
                <Stack>
                  {phone.percentage && (
                    <Typography
                      component="span"
                      variant="subtitle2"
                      color={red[500]}
                      fontWeight={700}
                    >
                      {numberWithSpaces(calcSale(phone.price, phone.percentage))} UAH
                    </Typography>
                  )}
                  <Typography
                    variant="subtitle2"
                    component="span"
                    sx={{
                      textDecoration: `${phone.percentage && "line-through"}`,
                      color: `${phone.percentage && "text.secondary"}`,
                    }}
                  >
                    {numberWithSpaces(phone.price)} UAH
                  </Typography>
                </Stack>
              </Stack>
            </CardActions>
          </Card>
        </LinkMUI>
      </Link>
    </Grid>
  );
};

export default ListPhonesItem;

import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { useEffect } from "react";
import useAuthContext from "../hooks/useAuthContext";

interface IPropsRequireAuth {
  children: JSX.Element[] | JSX.Element;
}

const RequireAuth: React.FC<IPropsRequireAuth> = (props) => {
  const router = useRouter();
  const {isAuth} = useAuthContext()
  
    if (!isAuth) {
     router.push("/")
    }


  return <>{props.children}</>;
};

export default RequireAuth;



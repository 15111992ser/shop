import React from "react";
import { IconButton, Typography } from "@mui/material";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import { Stack } from "@mui/system";

 export type formatLike = "like" | "dislike"

interface ILikeBlockProps {
  handleLikeReview: (format?: formatLike) => void;
  likesTotal: number;
  isLiked: boolean;
  format?: formatLike
}

export const LikeBlock: React.FC<ILikeBlockProps> = ({ handleLikeReview, likesTotal, isLiked, format = "like" }) => {
  return (
    <Stack
      direction="row"
      alignItems="center"
      sx={{ cursor: "pointer" }}
      onClick={()=>{handleLikeReview()}}
    >
      {likesTotal ? (
        <Typography component="span" color={`${isLiked ? "primary" : "action"}`}>
          {likesTotal}
        </Typography>
      ) : (
        ""
      )}
      <IconButton aria-label="like" sx={{ mx: 2 }} size="small">
        <ThumbUpIcon fontSize="small" color={`${isLiked ? "primary" : "action"}`} sx={{transform: `${format === "dislike" && " rotate(180deg)"}`}}/>
      </IconButton>
    </Stack>
  );
};

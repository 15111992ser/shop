import Button from "@mui/material/Button";
import { useForm, SubmitHandler } from "react-hook-form";
import {  Typography } from "@mui/material";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { LOGIN_USER } from "../common/api/gql/query/loginUser";
import { useLazyQuery } from "@apollo/client";
import useAuthContext from "../hooks/useAuthContext";
import { TFA_AUTHORIZATION } from "../common/api/gql/query/tfaAuthorization";
import { errorCodes } from "../constants/errors";
import { ShopTextField } from "./ShopTextField";
import useModalContext from "../hooks/useModalContext";

interface ILoginErrorException {
  code: string;
  message: string;
  additional: { tfa_enabled: boolean; tfa_token: string };
}

interface ISignInForm {
  email: string;
  password: string;
  secretCode?: string;
}

const schema = yup.object().shape({
  email: yup.string().email().required("Email is required"),
  password: yup.string().min(8).max(32).required("Password is required"),
});

export const LoginPage: React.FC = () => {
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<ISignInForm>({ resolver: yupResolver(schema) });

  const { login } = useAuthContext();
  const { setModal } = useModalContext();

  const [loginUser, { error: loginError }] = useLazyQuery(LOGIN_USER);
  const [tfaAuthorization, { error: tfaError }] = useLazyQuery(TFA_AUTHORIZATION);

  const loginErrorException = loginError?.graphQLErrors[0].extensions
    ?.exception as ILoginErrorException;

  const onSubmit: SubmitHandler<ISignInForm> = async (data) => {
    if (loginErrorException?.code !== errorCodes.tfa) {
      const res = await loginUser({
        variables: { password: data.password, email: data.email },
      });
      if (res?.data) {
        login(res.data.login);
        setModal({type: null});
      }
    }

    if (loginErrorException?.code === errorCodes.tfa) {
      const tfaToken = loginErrorException.additional.tfa_token;
      const tfaRes = await tfaAuthorization({
        variables: { token: String(data.secretCode) },
        context: { headers: { "tfa-token": tfaToken } },
      });
      if (tfaRes?.data) {
        login(tfaRes.data.tfaAuthorization);
        setModal({type: null});
      }
    }
  };

  return (
    <div>
      {(loginError?.message || tfaError?.message) && (
        <Typography sx={{ color: "red" }}>{tfaError?.message || loginError?.message}</Typography>
      )}
      <form onSubmit={handleSubmit(onSubmit)}>
        {loginErrorException?.code === errorCodes.tfa ? (
          <ShopTextField
            control={control}
            name="secretCode"
            error={!!errors.email?.message}
            label="enter secret code"
          />
        ) : (
          <>
            <ShopTextField
              control={control}
              name="email"
              error={!!errors.email?.message}
              helperText={errors.email?.message}
              label="email"
            />
            <ShopTextField
              control={control}
              name="password"
              type="password"
              error={!!errors.password?.message}
              helperText={errors.password?.message}
              label="password"
              size="medium"
            />
          </>
        )}

        <Button
          type="submit"
          variant="contained"
          fullWidth={true}
          disableElevation={true}
          size="large"
          sx={{
            marginTop: 2,
            marginBottom: 1,
          }}
        >
          Sign in
        </Button>
      </form>
    </div>
  );
};

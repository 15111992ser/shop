import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  Button,
  Link as LinkMUI,
  Avatar,
} from "@mui/material";
import { ShoppingBasket } from "@mui/icons-material";

import useAuthContext from "../hooks/useAuthContext";
import { blue } from "@mui/material/colors";

import Link from "next/link";
import { AuthModal } from "./AuthModal";
import useModalContext from "../hooks/useModalContext";
import { modalType } from "../context/modalContext";

export const Header = () => {
  const { logout, isAuth, user } = useAuthContext();
  const {setModal} = useModalContext()

  return (
    <AppBar>
      <Toolbar>
        <Link href={"/"}>
          <LinkMUI color="inherit" sx={{ flexGrow: 1, textDecoration: "none" }}>
            <Typography variant="h6" component="span">
              SHOP
            </Typography>
          </LinkMUI>
        </Link>
        {isAuth ? (
          <>
            <Link href={"/profile"}>
              <LinkMUI color="inherit" underline="none">
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  color="inherit"
                >
                  <Avatar sx={{ bgcolor: blue[500] }}>{user?.first_name[0].toUpperCase()}</Avatar>
                </IconButton>
              </LinkMUI>
            </Link>
            <Button
              color={"inherit"}
              onClick={() => {
                logout();
              }}
            >
              Logout
            </Button>
          </>
        ) : (
          <Button variant="text" onClick={() => {setModal({type: modalType.auth})}} color="inherit">
            SignIn/SignUp
          </Button>
        )}
        <IconButton color="inherit">
          <ShoppingBasket />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

import { Box, Container } from "@mui/material";
import Head from "next/head";
import React, { ReactNode } from "react";
import { Header } from "./Header";
import { grey } from "@mui/material/colors";
import { useEffect } from "react";
import useAuthContext from "../hooks/useAuthContext";
import { AuthModal } from "./AuthModal";

interface ILayoutProps {
  children: ReactNode;
}

const Layout: React.FC<ILayoutProps> = ({ children }) => {

  const { getUser } = useAuthContext();

  useEffect(() => {
    getUser();
  }, []);

  return (
    <>
      <Head>
        <meta charSet="utf-8"></meta>
        <meta name="keywords" content="smartphone, samsung, apple" />
        <meta name="description" content="This is an online smartphone store." />
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <Header />
      <main>
        <Box
          sx={{
            backgroundColor: grey[100],
            padding: "16px 0 16px",
            minHeight: "100vh",
            pt: "64px",
          }}
        >
          <Container maxWidth="xl">{children}</Container>
        </Box>
      </main>
      <AuthModal/>
    </>
  );
};

export default Layout;

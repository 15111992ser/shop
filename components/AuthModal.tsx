import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { FormGroup, FormControlLabel, Switch, Button, Dialog } from "@mui/material";
import { RegistrationPage } from "./Registration";
import { LoginPage } from "./Login";
import { useState } from "react";
import useModalContext from "../hooks/useModalContext";
import { modalType } from "../context/modalContext";

interface IAuthModalProps {

}

export const AuthModal: React.FC<IAuthModalProps> = () => {
  const {type, setModal} = useModalContext()
  const [isShowSignUp, setIsShowSignUp] = useState(false);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsShowSignUp(event.target.checked);
  };

  const handleClose = () => {
    setModal({type: null})
  };

  return (
    <>
      <Dialog open={type === modalType.auth} onClose={handleClose}>
        <FormGroup sx={{ px: 3, py: 1 }}>
          <FormControlLabel
            control={
              <Switch checked={isShowSignUp} onChange={handleChange} aria-label="login switch" />
            }
            label={isShowSignUp ? "SignUp" : "SignIn"}
          />
        </FormGroup>
        <DialogTitle sx={{ px: 3, py: 1 }}>Welcome</DialogTitle>
        <DialogContent>{isShowSignUp ? <RegistrationPage /> : <LoginPage />}</DialogContent>
      </Dialog>
    </>
  );
}

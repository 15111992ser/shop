import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import { useState } from "react";
import Carousel from "react-material-ui-carousel";

interface IPhoneSliderProps {
  pictures?: string[];
}

export const PhoneSlider: React.FC<IPhoneSliderProps> = ({ pictures }) => {
  const images = pictures || [];
  const [currentIndex, setCurrentIndex] = useState(0);

  return (
    <Box sx={{ p: 2, width: "100%" }}>
      <Carousel

        fullHeightHover={false}
        navButtonsProps={{
          style: {
            backgroundColor: "transparent"
          },
        }}
        navButtonsWrapperProps={{
          style: {
              bottom: '0',
              top: 'unset',
              height: "40px"
          }
      }} 
        NextIcon={
          <Button size="small" color="primary">
            Next
            <KeyboardArrowRight />
          </Button>
        }
        PrevIcon={
          <Button size="small" color="primary">
            <KeyboardArrowLeft />
            Back
          </Button>
        }
        index={currentIndex}
        interval={4000}
        animation="slide"
        autoPlay={false}
        swipe
        navButtonsAlwaysVisible
      >
        {images.map((img, index) => {
          return (
            <div key={index}>
              <Box
                component="img"
                sx={{
                  height: 350,
                  display: "block",
                  overflow: "hidden",
                  m: "0 auto",
                }}
                src={img}
              />
            </div>
          );
        })}
      </Carousel>
    </Box>
  );
};

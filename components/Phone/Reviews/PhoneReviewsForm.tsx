import React, { useEffect } from "react";
import { Button, Box, Rating, Stack, Typography } from "@mui/material";
import { ShopTextField } from "../../../components/ShopTextField";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { CREATE_REVIEW } from "../../../common/api/gql/mutation/createReview";
import { useMutation } from "@apollo/client";
import { UPDATE_REVIEW } from "../../../common/api/gql/mutation/updateReview";
import { red } from "@mui/material/colors";
import { getReviewsQueryData } from "./getQueryDataReviews";

interface IReviewsForm {
  rating: number;
  review: string;
}

const schema = yup.object().shape({
  review: yup.string().required("Review is required"),
  rating: yup.number().required("Rating is required"),
});

interface IPhoneReviewsFormProps {
  phoneId?: number;
  reviewId?: number;
  defaultValue?: {
    rating: number;
    review: string;
  };
  isEditMode?: boolean;
  setEditMode?: (editMode: boolean) => void;
}

export const PhoneReviewsForm: React.FC<IPhoneReviewsFormProps> = ({
  reviewId,
  phoneId,
  defaultValue = { rating: 0, review: "" },
  isEditMode,
  setEditMode,
}) => {
  
  const {
    reset,
    setFocus,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IReviewsForm>({
    resolver: yupResolver(schema),
    defaultValues: {
      rating: defaultValue.rating,
      review: defaultValue.review,
    },
  });

  useEffect(() => {
    setFocus("review", { shouldSelect: true });
  }, [setFocus]);

  const [createReview, { loading: loadingCreateReview, error: createReviewError }] = useMutation(
    CREATE_REVIEW,
    {
      refetchQueries: [getReviewsQueryData(phoneId)],
    }
  );

  const [updateReview, { loading: loadingUpdateReview, error: updateReviewError }] = useMutation(
    UPDATE_REVIEW,
    {
      refetchQueries: [getReviewsQueryData(phoneId)],
    }
  );

  const onSubmit: SubmitHandler<IReviewsForm> = async (data) => {
    const queryData = {
      rating: Number(data.rating),
      comment: data.review,
    }
    
    if (!isEditMode) {
      await createReview({
        variables: {
          data: {
            phone_id: Number(phoneId),
            ...queryData
          },
        },
      });
    } else {
      await updateReview({
        variables: {
          data: {
            review_id: Number(reviewId),
            ...queryData
          },
        },
      });
      setEditMode && setEditMode(false);
    }

    reset();
  };

  return (
    <Box>
      {(createReviewError?.message || updateReviewError?.message) && (
        <Typography sx={{ color: "red", fontSize: "14px" }}>
          {createReviewError?.message || updateReviewError?.message}
        </Typography>
      )}
      <form onSubmit={handleSubmit(onSubmit)}>
        <Stack direction="row" alignItems="center" m={1}>
          <Controller
            defaultValue={0}
            name="rating"
            control={control}
            render={({ field: { onChange, ref, ...field } }) => (
              <Rating onChange={onChange} ref={ref} {...field} size="large" />
            )}
          />
          {errors.rating?.message && (
            <Typography component="span" color={red[900]} mx={1}>
              {errors.rating?.message}
            </Typography>
          )}
        </Stack>

        <ShopTextField
          control={control}
          name="review"
          error={!!errors.review?.message}
          helperText={errors.review?.message}
          variant="outlined"
          multiline
          placeholder="Write your review here..."
        />

        <Button
          type="submit"
          variant="contained"
          size="large"
          sx={{
            marginTop: 2,
            marginBottom: 1,
          }}
          disabled={loadingCreateReview || loadingUpdateReview}
        >
          Send feedback
        </Button>
      </form>
    </Box>
  );
};

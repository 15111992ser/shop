import React, { useState } from "react";
import { Button, Paper, Stack, Typography, Box } from "@mui/material";
import MessageIcon from "@mui/icons-material/Message";
import useAuthContext from "../../../hooks/useAuthContext";
import { GET_REVIEWS } from "../../../common/api/gql/query/getReviews";
import { IReview } from "../../../types/review";
import { PhoneReviewsItem } from "./PhoneReviewsItem";
import { PhoneReviewsForm } from "./PhoneReviewsForm";
import { useQuery } from "@apollo/client";
import { reviewConstants } from "../../../constants/reviews";
import { AuthModal } from "../../AuthModal";
import useModalContext from "../../../hooks/useModalContext";
import { modalType } from "../../../context/modalContext";

interface IReviews {
  getReviews: {
    reviews: IReview[];
    total: number;
  };
}

interface IPhoneReviewsProps {
  phoneId?: number;
}

export const PhoneReviews: React.FC<IPhoneReviewsProps> = ({ phoneId }) => {
  const { user, isAuth } = useAuthContext();
  const { setModal } = useModalContext();
  const [isShowReviewsForm, setIsShowReviewsForm] = useState(false);

  const { data: reviewsData } = useQuery<IReviews>(GET_REVIEWS, {
    variables: {
      data: {
        phone_id: phoneId,
        take: reviewConstants.TAKE,
        skip: 0,
      },
    },
  });
  const reviews = [...(reviewsData?.getReviews.reviews || [])].reverse();

  return (
    <>
      <Paper sx={{ p: 2 }}>
        <Stack direction="row" justifyContent="space-between" alignItems="center">
          <Typography variant="h6">Reviews</Typography>
          <div>
            <Button
              onClick={() => {
                if (isAuth)  setIsShowReviewsForm(!isShowReviewsForm);
                else setModal({ type: modalType.auth });
              }}
              variant="contained"
              endIcon={<MessageIcon />}
            >
              Leave feedback
            </Button>
          </div>
        </Stack>
        {isShowReviewsForm && phoneId && (
          <Box
            sx={{
              border: (theme) => `1px solid ${theme.palette.grey[400]}`,
              p: 2,
              borderRadius: "4px",
              marginTop: "16px",
            }}
          >
            <PhoneReviewsForm phoneId={phoneId} />
          </Box>
        )}

        {reviews?.map((review) => {
          return <PhoneReviewsItem review={review} phoneId={phoneId} key={review.id} />;
        })}
      </Paper>
    </>
  );
};

import { Box, Divider, Stack, Typography } from "@mui/material";
import React from "react";
import { IComment } from "../../../../types/comment";
import { CommentItem } from "./CommentItem";

interface ICommentsListProps {
  comments?: IComment[];
  reviewId?: number;
  parentСommentId?: number;
  showLimit?: number;
}

export const CommentsList: React.FC<ICommentsListProps> = ({ comments, reviewId, parentСommentId, showLimit }) => {

  return (
    <Box sx={{ display: "flex" }}>
      <Divider orientation="vertical" flexItem sx={{ borderRightWidth: 3, mr: 2 }} />
      <Box
        sx={{
          flexGrow: 1,
        }}
      >
        {comments?.map((comment, i) => {
          return i < (showLimit || comments.length) && <CommentItem comment={comment} key={comment.id} reviewId={reviewId} parentСommentId={parentСommentId}/>;
        })}
      </Box>
    </Box>
  );
};

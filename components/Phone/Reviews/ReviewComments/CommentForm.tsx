import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { ShopTextField } from "../../../ShopTextField";
import { Button, Typography } from "@mui/material";
import { useMutation } from "@apollo/client";
import { CREATE_COMMENT } from "../../../../common/api/gql/mutation/createComment";
import { GET_COMMENTS } from "../../../../common/api/gql/query/getComments";
import { getCommentQueryData } from "./getCommentQueryData";
import { REPLY_TO_COMMENT } from "../../../../common/api/gql/mutation/replyToComment";
import { GET_REPLY_COMMENTS } from "../../../../common/api/gql/query/getReplyComments";
import { getSubCommentQueryData } from "./getSubCommentQueryData";

interface ICommentForm {
  comment: string;
}

const schema = yup.object().shape({
  comment: yup.string().required(),
});

interface ICommentProps {
  reviewId?: number;
  commentId?: number;
  isSubForm?: boolean;
  setIsShowCommentForm?: React.Dispatch<React.SetStateAction<boolean>>;
}

export const CommentForm: React.FC<ICommentProps> = ({
  reviewId,
  commentId,
  isSubForm,
  setIsShowCommentForm,
}) => {
  const {
    reset,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<ICommentForm>({
    resolver: yupResolver(schema),
  });

  const refetchComments = () => {
    return {
      query: GET_COMMENTS,
      variables: getCommentQueryData(reviewId),
    };
  };

  const [createComment, { data: commentData, error: createCommentError }] = useMutation(
    CREATE_COMMENT,
    {
      refetchQueries: [refetchComments()],
    }
  );

  const [replayToComment, { data: replayToCommentData, error: replayToCommenttError }] =
    useMutation(REPLY_TO_COMMENT, {
      refetchQueries: [
        {
          query: GET_REPLY_COMMENTS,
          variables: getSubCommentQueryData(commentId),
        },
        refetchComments(),
      ],
    });

  const onSubmit: SubmitHandler<ICommentForm> = async (data) => {
    if (isSubForm) {
      await replayToComment({
        variables: {
          data: {
            comment: data.comment,
            comment_id: Number(commentId),
          },
        },
      });
    } else {
      await createComment({
        variables: {
          data: {
            comment: data.comment,
            review_id: Number(reviewId),
          },
        },
      });
    }
    setIsShowCommentForm && setIsShowCommentForm(false);
    reset();
  };

  return (
    <>
      {createCommentError?.message && (
        <Typography component="p" sx={{ color: "red", fontSize: "14px" }}>
          {createCommentError?.message}
        </Typography>
      )}
      <form onSubmit={handleSubmit(onSubmit)}>
        <ShopTextField
          control={control}
          name="comment"
          error={!!errors.comment?.message}
          helperText={errors.comment?.message}
          variant="outlined"
          multiline
          placeholder="Write your comment here..."
        />

        <Button
          type="submit"
          variant="contained"
          size="large"
          sx={{
            marginTop: 2,
            marginBottom: 1,
          }}
        >
          Send comment
        </Button>
      </form>
    </>
  );
};

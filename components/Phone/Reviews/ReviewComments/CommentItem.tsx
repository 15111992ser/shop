import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/client";
import { Box, Divider, Stack, Typography, Button, IconButton } from "@mui/material";
import { RATE_COMMENT } from "../../../../common/api/gql/mutation/rateComment";
import { GET_COMMENTS } from "../../../../common/api/gql/query/getComments";
import { IComment } from "../../../../types/comment";
import { getFormatDate } from "../../../../utils/getFormatDate";
import { LikeBlock } from "../../../LikeBlock";
import { getCommentQueryData } from "./getCommentQueryData";
import ReplyIcon from "@mui/icons-material/Reply";
import useAuthContext from "../../../../hooks/useAuthContext";
import useModalContext from "../../../../hooks/useModalContext";
import { modalType } from "../../../../context/modalContext";
import { CommentForm } from "./CommentForm";
import { GET_REPLY_COMMENTS } from "../../../../common/api/gql/query/getReplyComments";
import { CommentsList } from "./CommentsList";
import { getSubCommentQueryData } from "./getSubCommentQueryData";

interface ICommentProps {
  comment: IComment;
  reviewId?: number;
  parentСommentId?: number;
}

export const CommentItem: React.FC<ICommentProps> = ({ comment, reviewId, parentСommentId }) => {
  const { isAuth } = useAuthContext();
  const { setModal } = useModalContext();
  const [isSowCommentForm, setShowCommentForm] = useState(false);

  const { data: subCommentsData } = useQuery(GET_REPLY_COMMENTS, {
    variables: getSubCommentQueryData(comment.id),
  });
  const subComments = subCommentsData?.getReplyComments.comments;

  const [likeComment] = useMutation(RATE_COMMENT, {
    refetchQueries: [
      {
        query: GET_COMMENTS,
        variables: getCommentQueryData(reviewId),
      },
      {
        query: GET_REPLY_COMMENTS,
        variables: getSubCommentQueryData(parentСommentId),
      },
    ],
  });

  const onClickLike = () => {
    if (isAuth) {
      likeComment({
        variables: {
          data: {
            comment_id: Number(comment.id),
            rating: true,
          },
        },
      });
    } else {
      setModal({ type: modalType.auth });
    }
  };

  return (
    <>
      <Box>
        <Stack direction="row" alignItems="center" px={2}>
          <Typography variant="subtitle1" component="p" fontWeight={700}>
            {comment?.user.first_name} {comment?.user.last_name}
          </Typography>
          <Typography variant="subtitle1" component="span" color="text.secondary" ml={1}>
            {getFormatDate(+comment.datetime)}
          </Typography>
        </Stack>
        <Typography variant="subtitle1" component="p" px={2}>
          {comment?.comment}
        </Typography>
        <Stack direction="row" justifyContent="space-between" alignItems="center">
          <Button
            onClick={() => {
              if (isAuth) setShowCommentForm(!isSowCommentForm);
              else setModal({ type: modalType.auth });
            }}
            size="small"
            variant="outlined"
            endIcon={<ReplyIcon />}
            sx={{ m: 2 }}
          >
            Replay
          </Button>
          <Typography component="span" >{comment.answers ? comment.answers + " answers" : ""} </Typography>
          <LikeBlock
            handleLikeReview={onClickLike}
            isLiked={comment.user_rating}
            likesTotal={comment.comment_rating}
          />
        </Stack>
      </Box>
      <Divider flexItem />
      {isSowCommentForm && (
        <Box px={2}>
          <CommentForm
            reviewId={reviewId}
            commentId={Number(comment.id)}
            setIsShowCommentForm={setShowCommentForm}
            isSubForm
          />
          <Divider flexItem />
        </Box>
      )}
      {subComments?.length ? (
        <Box m={2}>
          <CommentsList comments={subComments} reviewId={reviewId} parentСommentId={comment.id} />
        </Box>
      ) : (
        ""
      )}
    </>
  );
};

import { commentsConstants } from "../../../../constants/comments";

interface IGetCommentQueryDataResponse {
    data: {
      review_id?: number ;
      take: number;
      skip: number;
  };
}

export const getCommentQueryData = (reviewId?: number): IGetCommentQueryDataResponse => {
  return {
      data: {
        review_id: Number(reviewId), take: commentsConstants.TAKE, skip: 0 
    }
  };
};

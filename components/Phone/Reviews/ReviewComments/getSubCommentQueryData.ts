import { subCommentsConstants } from "../../../../constants/subComments";

interface IGetSubCommentQueryDataResponse {
    data: {
      comment_id?: number ;
      take: number;
      skip: number;
  };
}

export const getSubCommentQueryData = (commentId?: number): IGetSubCommentQueryDataResponse => {
  return {
      data: {
        comment_id: Number(commentId), take: subCommentsConstants.TAKE, skip: 0 
    }
  };
};

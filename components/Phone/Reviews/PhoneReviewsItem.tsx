import { Box, Stack, Typography, IconButton, Rating, Button } from "@mui/material";
import React, { useState } from "react";
import { IReview } from "../../../types/review";
import { getFormatDate } from "../../../utils/getFormatDate";
import DeleteIcon from "@mui/icons-material/Delete";
import { DELETE_REVIEW } from "../../../common/api/gql/mutation/deleteReview";
import useAuthContext from "../../../hooks/useAuthContext";
import EditIcon from "@mui/icons-material/Edit";
import { PhoneReviewsForm } from "./PhoneReviewsForm";
import { getReviewsQueryData } from "./getQueryDataReviews";
import ReplyIcon from "@mui/icons-material/Reply";
import { CommentsList } from "./ReviewComments/CommentsList";
import { GET_COMMENTS } from "../../../common/api/gql/query/getComments";
import { CommentForm } from "./ReviewComments/CommentForm";
import { getCommentQueryData } from "./ReviewComments/getCommentQueryData";
import { RATE_REVIEW } from "../../../common/api/gql/mutation/rateReview";
import { LikeBlock } from "../../LikeBlock";
import useModalContext from "../../../hooks/useModalContext";
import { modalType } from "../../../context/modalContext";
import { commentsConstants } from "../../../constants/comments";
import { useMutation, useQuery } from "@apollo/client";

interface IPhoneReviewsItemProps {
  review: IReview;
  phoneId?: number;
}

export const PhoneReviewsItem: React.FC<IPhoneReviewsItemProps> = ({ review, phoneId }) => {
  const { user, isAuth } = useAuthContext();
  const { setModal } = useModalContext();
  const [isEditMode, setEditMode] = useState(false);
  const [isHidden, setHidden] = useState(true);
  const [isShowCommentForm, setIsShowCommentForm] = useState(false);

  const [deleteReview, { loading, data, error }] = useMutation(DELETE_REVIEW, {
    refetchQueries: [getReviewsQueryData(phoneId)],
  });
  const [likeReview] = useMutation(RATE_REVIEW, {
    refetchQueries: [getReviewsQueryData(phoneId)],
  });

  const {
    data: dataComments,
    fetchMore,
  } = useQuery(GET_COMMENTS, {
    variables: getCommentQueryData(review.id),
  });

  let comments = dataComments?.getComments.comments;
  const commentsTotal = dataComments?.getComments.total;

  const handleShowMore = () => {
    if(comments?.length < commentsTotal){
      fetchMore({
        variables: {
          data: {
            review_id: Number(review.id),
            take: commentsTotal - commentsConstants.TAKE,
            skip: commentsConstants.TAKE,
          },
        },
      });
    }
    setHidden(false)
  };

  const handleHideComments = () => {
    setHidden(true)
  };

  const removeReview = () => {
    deleteReview({
      variables: {
        id: Number(review.id),
      },
    });
  };

  const onClickLike = () => {
    if (isAuth) {
      likeReview({
        variables: {
          data: {
            review_id: Number(review.id),
            rating: true,
          },
        },
      });
    } else {
      setModal({ type: modalType.auth });
    }
  };

  const editReview = () => {
    setEditMode(!isEditMode);
  };

  return (
    <>
      <Box
        sx={{
          mt: 2,
          border: (theme) => `1px solid ${theme.palette.grey[400]}`,
          borderRadius: "4px",
        }}
        key={review.id}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          sx={{ borderBottom: "1px solid #bdbdbd" }}
          px={2}
          height={40}
        >
          <Stack direction="row" alignItems="center">
            <Typography variant="subtitle1" component="p" fontWeight={700}>
              {review.user.first_name} {review.user.last_name}
            </Typography>
            <Typography variant="subtitle1" component="span" color="text.secondary" ml={1}>
              {`${review.updated ? "edited:" : ""}  ${getFormatDate(
                +review.updated || +review.datetime
              )}`}
            </Typography>
          </Stack>

          <Stack direction="row" alignItems="center">
            {Number(user?.id) === review.user.id && (
              <>
                <IconButton aria-label="delete" onClick={editReview}>
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="delete" onClick={removeReview}>
                  <DeleteIcon />
                </IconButton>
              </>
            )}
          </Stack>
        </Stack>
        <Stack direction="column" p={2}>
          {isEditMode ? (
            <PhoneReviewsForm
              defaultValue={{ review: review.comment, rating: review.rating }}
              isEditMode
              setEditMode={(editMode) => {
                setEditMode(editMode);
              }}
              reviewId={review.id}
              phoneId={phoneId}
            />
          ) : (
            <>
              <Rating name="read-only" value={review.rating} readOnly size="medium" />
              <Typography variant="body1" component="p" mt={1} sx={{ wordBreak: "break-all" }}>
                {review.comment}
              </Typography>
            </>
          )}
        </Stack>
        <Stack direction="row" justifyContent="space-between" alignItems="center">
          <Button
            onClick={() => {
              if (isAuth) setIsShowCommentForm(!isShowCommentForm);
              else setModal({ type: modalType.auth });
            }}
            variant="outlined"
            endIcon={<ReplyIcon />}
            sx={{ m: 2, mt: 0 }}
          >
            Replay
          </Button>
          <LikeBlock
            handleLikeReview={onClickLike}
            isLiked={review.user_rating}
            likesTotal={review.review_rating}
          />
        </Stack>
      </Box>
      {isShowCommentForm && (
        <Box>
          <CommentForm reviewId={review.id} setIsShowCommentForm={setIsShowCommentForm} />
        </Box>
      )}

      {comments?.length ? (
        <Box mt={2}>
          <CommentsList comments={comments} reviewId={review.id} showLimit={isHidden ? commentsConstants.TAKE : undefined }/>
        </Box>
      ) : (
        ""
      )}
      <Stack direction="row" justifyContent="center">
        {commentsConstants.TAKE < commentsTotal && (
          <Button
            variant="text"
            size="small"
            onClick={() => {
              if (isHidden) handleShowMore();
              else handleHideComments()
            }}
          >
            {`${isHidden ? "Show" : "Hide"} all ${commentsTotal} comments`}
          </Button>
        )}
      </Stack>
    </>
  );
};

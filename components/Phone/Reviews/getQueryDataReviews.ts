import { DocumentNode } from "@apollo/client";
import { GET_REVIEWS } from "../../../common/api/gql/query/getReviews";
import { reviewConstants } from "../../../constants/reviews";

interface IGetReviewsQueryDataResponse {
  query: DocumentNode;
  variables: {
    data: {
      phone_id?: number ;
      take: number;
      skip: number;
    };
  };
}

export const getReviewsQueryData = (phoneId?: number): IGetReviewsQueryDataResponse => {
  return {
    query: GET_REVIEWS,
    variables: {
      data: {
        phone_id: phoneId,
        take: reviewConstants.TAKE,
        skip: 0,
      },
    },
  };
};

import { Button, Rating, Stack, Typography, Box } from "@mui/material";
import React from "react";
import { IPhone } from "../../types/phones";
import { numberWithSpaces } from "../../utils/numberWithSpaces";
import { red } from "@mui/material/colors";
import { getFormatDate } from "../../utils/getFormatDate";
import { dateDifferenceDate } from "../../utils/dateDifferenceDate";
import { calcSale } from "../../utils/calcSale";

interface IPhoneDescriptionProps {
  phone?: IPhone;
}

export const PhoneDescription: React.FC<IPhoneDescriptionProps> = ({ phone }) => {
  return (
    <Stack direction="column" justifyContent="space-between" sx={{ height: "100%" }}>
      <Typography variant="h6">{phone?.name}</Typography>
      <Stack direction="row" alignItems="center">
        <Typography variant="body1" component="p" mr={1}>
          Rating:
        </Typography>
        {phone?.rating && (
          <Rating name="read-only" value={phone.rating} readOnly size="small" precision={0.1} />
        )}
      </Stack>
      {phone?.percentage && (
        <Box>
          <Stack direction="row" alignItems="center">
            <Typography variant="h6" color={red[900]} component="span">
              SALE: -{phone.percentage}%
            </Typography>
            <Typography variant="body1" component="span" ml={1}>
              ({getFormatDate(phone.start_time)} - {getFormatDate(phone.end_time)})
            </Typography>
            <Typography variant="body1" component="span" ml={1}></Typography>
          </Stack>
          <Typography variant="body1" component="p">
            Hurry up, you have{" "}
            <Typography variant="h6" color={red[900]} component="span">
              {dateDifferenceDate(phone.start_time, phone.end_time)}
            </Typography>{" "}
            days left
          </Typography>
        </Box>
      )}
      <Typography variant="body1" component="p">
        Memory:
        <Typography component="span" mx={1} sx={{ fontWeight: 700 }}>
          {phone?.memory} GB
        </Typography>
      </Typography>
      <Typography variant="body1" component="p">
        Ram:
        <Typography component="span" mx={1} sx={{ fontWeight: 700 }}>
          {phone?.ram} GB
        </Typography>
      </Typography>
      <Typography variant="body1" component="p">
        Diagonal:
        <Typography component="span" mx={1} sx={{ fontWeight: 700 }}>
          {phone?.diagonal}"
        </Typography>
      </Typography>
      <Typography variant="body1" component="p">
        OS: {phone?.os}
        <Typography component="span" mx={1} sx={{ fontWeight: 700 }}>
          OS: {phone?.os}
        </Typography>
      </Typography>
      <Stack direction="row" alignItems="center">
        <Typography variant="body1" component="span">
          Price:
        </Typography>
        <Typography
          mx={1}
          component="span"
          fontWeight={700}
          sx={{
            textDecoration: `${phone?.percentage && "line-through"}`,
            fontWeight: `${!phone?.percentage && "700"}`,
            color: `${phone?.percentage && "text.secondary"}`,
          }}
        >
          {phone && numberWithSpaces(phone.price)} UAH
        </Typography>
        {phone?.percentage && (
          <Typography component="span" variant="h6" color={red[500]} fontWeight={700}>
            {phone && numberWithSpaces(calcSale(phone.price, phone.percentage))} UAH
          </Typography>
        )}
      </Stack>
      <Button variant="contained" size="large" sx={{ width: "50%", margin: "0 auto" }}>
        Buy
      </Button>
    </Stack>
  );
};

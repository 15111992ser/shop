import {
  Paper,
  Grid,
  Typography,
} from "@mui/material";
import { IPhone } from "../../types/phones";
import { PhoneSlider } from "./PhoneSlider";
import { PhoneReviews } from "./Reviews/PhoneReviews";
import { PhoneDescription } from "./PhoneDescription";

interface IPhoneProps {
  phone: IPhone;
}
 const Phone: React.FC<IPhoneProps> = ({phone}) => {

  return (
    <Grid container spacing={2} mt={1}>
      <Grid item xs={12}>
        <Paper sx={{ p: 1 }}>
          <Typography variant="h5">All about the product</Typography>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Paper>
              <PhoneSlider pictures={phone?.phone_images.images} />
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper sx={{ height: "100%", p: 2 }}>
              <PhoneDescription phone={phone}/>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <PhoneReviews phoneId={Number(phone.id)} />
      </Grid>
    </Grid>
  );
};

export default Phone
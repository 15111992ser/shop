import * as React from "react";
import styled from '@emotion/styled';
import { red } from "@mui/material/colors";
import { Typography } from "@mui/material";

const SaleBlock = styled.div`
  color: darkslategray;
  background: ${red[400]};
  opacity: 0.9;
  padding: 8px;
  border-radius: 4px;
  position: absolute;
  left: 8px;
  top: 8px;
`;

interface ISaleProps {
  sale: number;
}

export const Sale: React.FC<ISaleProps> = ({ sale }) => {
  return (
    <SaleBlock>
      <Typography variant="h6" color={red[900]} fontWeight={700}>
        SALE - {sale}%
      </Typography>
    </SaleBlock>
  );
};

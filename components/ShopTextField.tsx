import React from "react";
import { Controller, Control } from "react-hook-form";
import { TextField } from "@mui/material";

interface IShopTextFieldProps {
  control: any;
  name: string;
  error?: boolean;
  label?: string;
  helperText?: string;
  variant?: "standard" | "outlined" | "filled";
  size?: "small" | "medium";
  margin?: "none" | "normal" | "dense";
  type?: string;
  multiline?: boolean;
  placeholder?: string;
}

export const ShopTextField: React.FC<IShopTextFieldProps> = ({
  control,
  name,
  error,
  label,
  helperText,
  size = "medium",
  margin = "dense",
  variant = "standard",
  type,
  multiline,
  placeholder
}) => {
  return (
    <Controller
      defaultValue=""
      control={control}
      name={name}
      render={({ field: { ref, ...field } }) => (
        <TextField
          placeholder={placeholder}
          multiline={multiline}
          {...field}
          inputRef={ref}
          variant={variant}
          size={size}
          margin={margin}
          label={label}
          onChange={(e) => field.onChange(e)}
          fullWidth={true}
          error={error}
          helperText={helperText}
          type={type}
        />
      )}
    />
  );
};

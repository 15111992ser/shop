import { useMutation } from "@apollo/client";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button,  Typography } from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import * as yup from "yup";
import "yup-phone";
import { REGISTER_USER } from "../common/api/gql/mutation/registrationUser";
import useAuthContext from "../hooks/useAuthContext";
import useModalContext from "../hooks/useModalContext";
import { ShopTextField } from "./ShopTextField";

interface IRegistrationForm {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phoneNumber: string;
}

const schema = yup.object().shape({
  firstName: yup.string().required("First name is required"),
  lastName: yup.string(),
  email: yup.string().email().required("Email is required"),
  password: yup.string().min(8).max(32).required("Password is required"),
  phoneNumber: yup.string().phone().required("Phone number is required"),
});

export const RegistrationPage = () => {
  const { login } = useAuthContext();
  const { setModal } = useModalContext();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IRegistrationForm>({ resolver: yupResolver(schema), mode: "onBlur" });

  const [addUser, { error, loading }] = useMutation(REGISTER_USER);

  const onSubmit: SubmitHandler<IRegistrationForm> = async (data) => {
    const res = await addUser({
      variables: {
        data: {
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.email,
          password: data.password,
          phone_number: data.phoneNumber,
        },
      },
    });

    if (res.data) {
      login(res.data.registration);
    }
    setModal({type: null});
  };

  return (
    <div>
      {error?.message && <Typography sx={{ color: "red" }}>{error?.message}</Typography>}
      <form onSubmit={handleSubmit(onSubmit)}>
        <ShopTextField
          control={control}
          name="firstName"
          error={!!errors.firstName?.message}
          helperText={errors.firstName?.message}
          label="First name"
        />
        <ShopTextField
          control={control}
          name="lastName"
          error={!!errors.lastName?.message}
          helperText={errors.lastName?.message}
          label="Last name"
        />
        <ShopTextField
          control={control}
          name="email"
          error={!!errors.email?.message}
          helperText={errors.email?.message}
          label="Email"
        />

        <ShopTextField
          control={control}
          name="password"
          error={!!errors.password?.message}
          helperText={errors.password?.message}
          label="Password"
          type="password"
        />

        <ShopTextField
          control={control}
          name="phoneNumber"
          error={!!errors.phoneNumber?.message}
          helperText={errors.phoneNumber?.message}
          label="Phone number"
          type="tel"
        />
        
        <Button
          type="submit"
          variant="contained"
          fullWidth={true}
          disabled={loading}
          disableElevation={true}
          size="large"
          sx={{
            marginTop: 2,
            marginBottom: 1,
          }}
        >
          Sign up
        </Button>
      </form>
    </div>
  );
};

import { useLazyQuery } from "@apollo/client";
import React, { ReactNode, useEffect, useReducer, useState } from "react";
import { GET_USER_INFO } from "../common/api/gql/query/getUserInfo";
import { IUser } from "../types/user";
import cookie from 'js-cookie'

export enum modalType{
  "auth" 
}

interface IModalContext {
  type: modalType | null;
  setModal: ({type}: {type:modalType | null})=> void
}

export const ModalContext = React.createContext<IModalContext>({
  type: null,
  setModal: () => {}
});

const ModalContextProvider = ({ children }: {children: ReactNode}) => {
const [type, setType] = useState<modalType | null>(null)

const setModal = ({type}: {type:modalType | null}) => {
  setType(type)
}

  return (
    <ModalContext.Provider
      value={{type, setModal}}
    >
      {children}
    </ModalContext.Provider>
  );
};

export default ModalContextProvider;

import { useLazyQuery } from "@apollo/client";
import React, { useEffect, useReducer } from "react";
import { GET_USER_INFO } from "../common/api/gql/query/getUserInfo";
import { IUser } from "../types/user";
import cookie from 'js-cookie'

interface IGetUserInfo {
  getUserInfo: IUser;
}

interface IContext {
  isAuth: boolean;
  user: IUser | null;
  login: (data: IUser) => void;
  logout: () => void;
  getUser: () => void;
  refetchUser: () => void;
}

interface IAuthContextProvider {
  children: JSX.Element;
}

export const AuthContext = React.createContext<IContext>({
  isAuth: false,
  user: null,
  login: (userData: IUser) => {},
  logout: () => {},
  getUser: () => {},
  refetchUser: () => {},
});

const initialState = { user: null, isAuth: false };

interface IAuthAction {
  type: "LOGIN" | "LOGOUT";
  payload?: any;
}

const authReducer = (state = initialState, action: IAuthAction) => {
  switch (action.type) {
    case "LOGIN":
      return { ...state, user: action.payload, isAuth: true };
    case "LOGOUT":
      return { ...state, user: null, isAuth: false };
    default:
      return state;
  }
};

const AuthContextProvider: React.FC<IAuthContextProvider> = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const [getUser, {data, refetch: refetchUser}] = useLazyQuery<IGetUserInfo>(GET_USER_INFO, {
    fetchPolicy: "network-only",
  });

  const setUser = async () => {
    try {
      if (cookie.get("access-token")) {
        if (data) {
          login(data?.getUserInfo);
        }
      }
    } catch {
      logout();
    }
  };

  useEffect(() => {
    setUser();
  }, [data]);

  const login = (userData: IUser) => {
    dispatch({
      type: "LOGIN",
      payload: userData,
    });

    if (userData?.token) {
      cookie.set("access-token", userData.token, { expires: 7 })
    }
  };

  const logout = () => {
    dispatch({
      type: "LOGOUT",
    });
    cookie.remove("access-token", { path: '' })
  };

  return (
    <AuthContext.Provider
      value={{ user: state.user, isAuth: state.isAuth, login, logout, getUser, refetchUser }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;

import { GetStaticPropsContext } from "next";
import React from "react";
import { client } from "../../common/api/apolloClient";
import { GET_PHONE } from "../../common/api/gql/query/getPhone";
import Phone from "../../components/Phone/Phone";
import { phone } from "../../constants/phones";
import { IPhone } from "../../types/phones";

interface IPhonePageProps {
  phone: IPhone
}

const PhonePage: React.FC<IPhonePageProps> = ({ phone }) => {
  return (
    <>
      <Phone phone={phone} />
    </>
  );
};

export default PhonePage;

export async function getServerSideProps({ params }: GetStaticPropsContext) {
  const { data } = await client.query({
    query: GET_PHONE,
    variables: { id: Number(params?.id) },
  });

  return {
    props: {
      phone: data.getPhone,
    },
  };
}

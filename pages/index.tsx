import type { GetStaticPropsContext, NextPage } from "next";
import { GET_FILTERS } from "../common/api/gql/query/getFilters";
import { phone } from "../constants/phones";
import Shop from "../components/Shop/Shop";
import { client } from "../common/api/apolloClient";
import Head from "next/head";
import { IFilters } from "../types/phones";

interface IIndexProps{
  getFilters: IFilters
}

const Index: NextPage<IIndexProps> = ({getFilters}) => {
  return (
    <>
      <Head>
        <title key="title">Shop</title>
      </Head>
      <Shop filters={getFilters} />
    </>
  );
};

export default Index;

export async function getServerSideProps(context: GetStaticPropsContext) {
  const { data: filters } = await client.query({
    query: GET_FILTERS,
    variables: {
      data: {
        name: "",
        min_price: 0,
        max_price: phone.MAX_PRICE,
      },
    },
  });

  return {
    props: { getFilters: filters.getFilters },
  };
}

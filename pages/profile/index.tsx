import Head from "next/head";
import { TFaBlock } from "../../components/Profile/TFaBlock";
import RequireAuth from "../../components/RequireAuth";

const Profile = () => {

  return (
    <RequireAuth>
      <Head>
        <title key="title">Profile | Shop</title>
      </Head>
      <TFaBlock />
    </RequireAuth>
  );
};

export default Profile;

